## Installation
1. Download **[Latest Version](https://gitlab.com/Artur91425/Blizzard_DeathRecap/-/archive/master/Blizzard_DeathRecap-master.zip)**
2. Unpack the Zip file
4. Copy "!Blizzard_DeathRecap" into Wow-Directory\Interface\AddOns
5. Restart Wow

## Screenshots
<img src="https://gitlab.com/Artur91425/Blizzard_DeathRecap/uploads/96f4308094d339a97c755773a93a1717/Untitled.png" width="48%">
<img src="https://gitlab.com/Artur91425/Blizzard_DeathRecap/uploads/e82b56a3071ce0715c148859adc2ff84/Untitled2.jpg" width="48%">
