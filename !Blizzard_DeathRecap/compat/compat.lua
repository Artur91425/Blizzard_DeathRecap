local __recapID = 0 -- global recap counter

local f = CreateFrame("Frame")
f:Hide()
f:SetScript('OnEvent', function(self, event, ...)
  if not self[event] then return end
  self[event](self, ...)
end)

-- add print into DEFAULT_CHAT_FRAME
------------------------------------------------------
f:RegisterEvent("PLAYER_DEAD")
function f:PLAYER_DEAD()
  __recapID = __recapID + 1
  local recapID = __recapID
  local recapLink = GetDeathRecapLink(recapID)
  for i=1,NUM_CHAT_WINDOWS do
    local chatFrame = _G["ChatFrame"..i]
    if chatFrame and chatFrame:IsEventRegistered("CHAT_MSG_SYSTEM") and i ~= 2 then
      ChatFrame_OnEvent(chatFrame, "CHAT_MSG_SYSTEM", recapLink, "", "", "", "", "", 0, 0, "", 0, 0, nil, 0, false)
    end
  end
end

-- fix DeathRecapFrame point
------------------------------------------------------
f:RegisterEvent("VARIABLES_LOADED")
function f:VARIABLES_LOADED()
  DeathRecapFrame:HookScript('OnShow', function(self)
    self:ClearAllPoints()
    self:SetPoint('TOP', 0, -116)
  end)
end

-- FrameXML\ItemRef.lua
------------------------------------------------------
local orig_SetItemRef = SetItemRef
function SetItemRef(link, text, button, chatFrame)
  if ( strsub(link, 1, 5) == "death" ) then
    local _, id = strsplit(":", link);
    id = tonumber(id)
    OpenDeathRecapUI(id);
    return;
  end
  orig_SetItemRef(link, text, button, chatFrame)
end

-- FrameXML\StaticPopup.lua
------------------------------------------------------
StaticPopupDialogs["DEATH"] = {
  text = DEATH_RELEASE_TIMER,
  button1 = DEATH_RELEASE,
  button2 = USE_SOULSTONE,
  button3 = DEATH_RECAP,
  OnShow = function(self)
    self.timeleft = GetReleaseTimeRemaining();
    local text = HasSoulstone();
    if ( text ) then
      self.button2:SetText(text);
    end

    if ( IsActiveBattlefieldArena() ) then
      self.text:SetText(DEATH_RELEASE_SPECTATOR);
    elseif ( self.timeleft == -1 ) then
      self.text:SetText(DEATH_RELEASE_NOTIMER);
    end
    if ( not self.UpdateRecapButton ) then
      self.UpdateRecapButton = function( self )
        if ( DeathRecap_HasEvents() ) then
          self.button3:Enable();
          self.button3:SetScript("OnEnter", nil );
          self.button3:SetScript("OnLeave", nil);
        else
          self.button3:Disable();
          self.button3:SetMotionScriptsWhileDisabled(true);
          self.button3:SetScript("OnEnter", function(self)
            GameTooltip:SetOwner(self, "ANCHOR_BOTTOMRIGHT");
            GameTooltip:SetText(DEATH_RECAP_UNAVAILABLE);
            GameTooltip:Show();
          end );
          self.button3:SetScript("OnLeave", GameTooltip_Hide);
        end
      end
    end

    self:UpdateRecapButton();
  end,
  OnHide = function(self)
    self.button3:SetScript("OnEnter", nil );
    self.button3:SetScript("OnLeave", nil);
    self.button3:SetMotionScriptsWhileDisabled(false);
  end,
  OnAccept = function(self)
    if ( IsActiveBattlefieldArena() ) then
      local info = ChatTypeInfo["SYSTEM"];
      DEFAULT_CHAT_FRAME:AddMessage(ARENA_SPECTATOR, info.r, info.g, info.b, info.id);
    end
    RepopMe();
    if ( CannotBeResurrected() ) then
      return 1
    end
  end,
  OnCancel = function(self, data, reason)
    if ( reason == "override" ) then
      return;
    end
    if ( reason == "timeout" ) then
      return;
    end
    if ( reason == "clicked" ) then
      if ( HasSoulstone() ) then
        UseSoulstone();
      else
        RepopMe();
      end
      if ( CannotBeResurrected() ) then
        return 1
      end
    end
  end,
  OnAlt = function()
    OpenDeathRecapUI();
  end,
  OnUpdate = function(self, elapsed)
    if ( IsFalling() and (not IsOutOfBounds()) ) then
      self.button1:Disable();
      self.button2:Disable();
      return;
    end

    self.button1:Enable();

    if( HasSoulstone() ) then
      self.button2:Enable();
    else
      self.button2:Disable();
    end

    if ( self.UpdateRecapButton) then
      self:UpdateRecapButton();
    end
  end,
  DisplayButton2 = function(self)
    return HasSoulstone();
  end,

  timeout = 0,
  whileDead = 1,
  interruptCinematic = 1,
  notClosableByLogout = 1,
  noCancelOnReuse = 1,
  noCloseOnAlt = true,
  cancels = "RECOVER_CORPSE"
};

-- add support noCloseOnAlt
hooksecurefunc("StaticPopup_OnClick", function(dialog, index)
  local which = dialog.which;
  local info = StaticPopupDialogs[which];
  if not dialog:IsShown() and index == 3 and info.noCloseOnAlt then
    dialog:Show();
  end
end)

-- fix stupid bug with button3 on 3.3.5...
local tempButtonLocs = {};  --So we don't make a new table each time.
hooksecurefunc("StaticPopup_Show", function(which, text_arg1, text_arg2, data)
  local dialog = StaticPopup_FindVisible(which, data)
  if not dialog then return end
  local info = StaticPopupDialogs[which];

  -- Set the buttons of the dialog
  local button1 = _G[dialog:GetName().."Button1"];
  local button2 = _G[dialog:GetName().."Button2"];
  local button3 = _G[dialog:GetName().."Button3"];

  do  --If there is any recursion in this block, we may get errors (tempButtonLocs is static). If you have to recurse, we'll have to create a new table each time.
    assert(#tempButtonLocs == 0); --If this fails, we're recursing. (See the table.wipe at the end of the block)

    tinsert(tempButtonLocs, button1);
    tinsert(tempButtonLocs, button2);
    tinsert(tempButtonLocs, button3);

    for i=#tempButtonLocs, 1, -1 do
      --Do this stuff before we move it. (This is why we go back-to-front)
      tempButtonLocs[i]:SetText(info["button"..i]);
      tempButtonLocs[i]:Hide();
      tempButtonLocs[i]:ClearAllPoints();
      --Now we possibly remove it.
      if ( not (info["button"..i] and ( not info["DisplayButton"..i] or info["DisplayButton"..i](dialog))) ) then
        tremove(tempButtonLocs, i);
      end
    end

    local numButtons = #tempButtonLocs;
    --Save off the number of buttons.
    dialog.numButtons = numButtons;

    if ( numButtons == 3 ) then
      tempButtonLocs[1]:SetPoint("BOTTOMRIGHT", dialog, "BOTTOM", -72, 16);
    elseif ( numButtons == 2 ) then
      tempButtonLocs[1]:SetPoint("BOTTOMRIGHT", dialog, "BOTTOM", -6, 16);
    elseif ( numButtons == 1 ) then
      tempButtonLocs[1]:SetPoint("BOTTOM", dialog, "BOTTOM", 0, 16);
    end

    for i=1, numButtons do
      if ( i > 1 ) then
        tempButtonLocs[i]:SetPoint("LEFT", tempButtonLocs[i-1], "RIGHT", 13, 0);
      end

      local width = tempButtonLocs[i]:GetTextWidth();
      if ( width > 110 ) then
        tempButtonLocs[i]:SetWidth(width + 20);
      else
        tempButtonLocs[i]:SetWidth(120);
      end
      tempButtonLocs[i]:Enable();
      tempButtonLocs[i]:Show();
    end

    table.wipe(tempButtonLocs);
  end

  return dialog
end)

-- FrameXML\UIParent.lua
------------------------------------------------------
-- Frames using the new Templates
UIPanelWindows["DeathRecapFrame"] =       { area = "center",      pushable = 0, whileDead = 1, allowOtherPanels = 1};

-- function DeathRecap_LoadUI()
--   UIParentLoadAddOn("DeathRecap");
-- end

function OpenDeathRecapUI(id)
  -- if (not DeathRecapFrame) then
  --   DeathRecap_LoadUI();
  -- end
  DeathRecapFrame_OpenRecap(id);
end

function BreakUpLargeNumbers(value)
  local retString = "";
  if ( value < 1000 ) then
    if ( (value - math.floor(value)) == 0) then
      return value;
    end
    local decimal = (math.floor(value*100));
    retString = string.sub(decimal, 1, -3);
    retString = retString..DECIMAL_SEPERATOR;
    retString = retString..string.sub(decimal, -2);
    return retString;
  end

  value = math.floor(value);
  local strLen = strlen(value);
  if ( GetCVarBool("breakUpLargeNumbers") ) then
    if ( strLen > 6 ) then
      retString = string.sub(value, 1, -7)..LARGE_NUMBER_SEPERATOR;
    end
    if ( strLen > 3 ) then
      retString = retString..string.sub(value, -6, -4)..LARGE_NUMBER_SEPERATOR;
    end
    retString = retString..string.sub(value, -3, -1);
  else
    retString = value;
  end
  return retString;
end

-- AddOns\Blizzard_CombatLog\Blizzard_CombatLog.lua
------------------------------------------------------
-- local orig_CombatLog_OnEvent = CombatLog_OnEvent
-- function CombatLog_OnEvent(filterSettings, timestamp, event, sourceGUID, sourceName, sourceFlags, destGUID, destName, destFlags, ...)
--   if ( event == "UNIT_DIED" or event == "UNIT_DESTROYED" or event == "UNIT_DISSIPATES" ) then
--     local recapID = __recapID;
--     -- handle death recaps
--     if ( destGUID == UnitGUID("player") ) then
--       local lineColor = COMBATLOG_DEFAULT_COLORS.unitColoring[COMBATLOG_FILTER_MINE];
--       return GetDeathRecapLink(recapID), lineColor.r, lineColor.g, lineColor.b;
--     end
--   end
--   orig_CombatLog_OnEvent(filterSettings, timestamp, event, sourceGUID, sourceName, sourceFlags, destGUID, destName, destFlags, ...)
-- end
