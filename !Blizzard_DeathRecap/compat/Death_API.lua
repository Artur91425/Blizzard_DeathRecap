local error = error
local tinsert, tremove, twipe = table.insert, table.remove, table.wipe
local CreateFrame, UnitGUID, UnitIsDead = CreateFrame, UnitGUID, UnitIsDead

local playerGUID
local history = {}
local events = {}
local f = CreateFrame("Frame")
f:Hide()
f:SetScript('OnEvent', function(self, event, ...)
  if not self[event] then return end
  self[event](self, ...)
end)

-- helper functions
------------------------------------------------------
f:RegisterEvent("PLAYER_ENTERING_WORLD")
function f:PLAYER_ENTERING_WORLD()
  playerGUID = UnitGUID("player")
end
f:RegisterEvent("PLAYER_REGEN_ENABLED")
function f:PLAYER_REGEN_ENABLED()
  twipe(history)
end
f:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
function f:COMBAT_LOG_EVENT_UNFILTERED(timestamp, event, sourceGUID, sourceName, sourceFlags, destGUID, destName, destFlags, ...)
  if destGUID ~= playerGUID then return end

  local hideCaster = false
  -- Spell standard order
  local spellId, spellName, spellSchool;
  -- Damage standard order
  local amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing, overhealing;

  if ( event == "SWING_DAMAGE" ) then
    -- Damage standard
    amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing = ...;
  elseif ( event == "SPELL_DAMAGE" or event == "SPELL_BUILDING_DAMAGE" ) then
    -- Damage standard
    spellId, spellName, spellSchool, amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing = ...;
  elseif ( event == "SPELL_PERIODIC_DAMAGE" ) then
    -- Damage standard
    spellId, spellName, spellSchool, amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing = ...;
  elseif ( event == "RANGE_DAMAGE" ) then
    -- Damage standard
    spellId, spellName, spellSchool, amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing = ...;
  elseif ( event == "DAMAGE_SHIELD" ) then  -- Damage Shields
    -- Spell standard, Damage standard
    spellId, spellName, spellSchool, amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing = ...;
  elseif ( event == "ENVIRONMENTAL_DAMAGE" ) then
    --Environemental Type, Damage standard
    environmentalType, amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing = ...;
    hideCaster = true
  elseif ( event == "DAMAGE_SPLIT" ) then
    -- Spell Standard Arguments, Damage standard
    spellId, spellName, spellSchool, amount, overkill, school, resisted, blocked, absorbed, critical, glancing, crushing = ...;
  elseif ( event == "UNIT_DIED" or event == "UNIT_DESTROYED" or event == "UNIT_DISSIPATES" ) then
    tinsert(events, CopyTable(history))
    twipe(history)
    return
  else
    return
  end

  if UnitIsDead("player") then return end

  local data = {
    timestamp = timestamp,
    event = event,
    hideCaster = hideCaster,
    sourceGUID = sourceGUID,
    sourceName = sourceName,
    sourceFlags = sourceFlags,
    sourceRaidFlags = 0,
    destGUID = destGUID,
    destName = destName,
    destFlags = destFlags,
    destRaidFlags = 0,
    spellName = spellName,
    spellId = spellId,
    environmentalType = environmentalType,
    isOffHand = false,
    amount = amount,
    overkill = overkill,
    school = school,
    resisted = resisted,
    blocked = blocked,
    absorbed = absorbed,
    critical = critical or false,
    glancing = glancing or false,
    crushing = crushing or false,
    currentHP = UnitHealth("player"),
  }
  tinsert(history, 1, data)
  if #history > 5 then
    tremove(history)
  end
end

-- API
------------------------------------------------------
DeathRecap_GetEvents = DeathRecap_GetEvents or function(recapID)
  recapID = recapID or #events
  return events[recapID]
end

DeathRecap_HasEvents = DeathRecap_HasEvents or function()
  return #events > 0
end

GetDeathRecapLink = GetDeathRecapLink or function(recapID)
  if not recapID then
    error("Usage: GetDeathRecapLink(recapID)", 2)
  end
  return DEATH_RECAP_LINK:format(recapID)
end
